package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"strings"
	"os"

	"github.com/google/oauth2l/go/oauth2client"
)

func help() {
	fmt.Println("Usage: mnitoken {fetch|header|token} <key.json>")
}

var commands = map[string]func(*oauth2client.Token){
	"fetch":  func(token *oauth2client.Token) {
		fmt.Println(token.AccessToken)
	},
	"header": func(token *oauth2client.Token) {
		fmt.Printf("Authorization: %s %s\n", token.TokenType, token.AccessToken)
	},
	"token":  func(token *oauth2client.Token) {
		jsonStr, err := json.MarshalIndent(token, "", "  ")
		if err != nil {
			panic("Failed to covert token to json: " + err.Error())
		}
		fmt.Println(string(jsonStr))
	},
}

func main() {
	helpFlag := flag.Bool("h", false, "")

	flag.Parse()
	args := flag.Args()

	if *helpFlag || len(args) < 2 {
		help()
		return
	}

	jsonFile := args[1]
	secretBytes, err := ioutil.ReadFile(jsonFile)
	if err != nil {
		fmt.Printf("Failed to read file %s.\n", jsonFile)
		os.Exit(-1)
		return
	}

	cmdFunc, ok := commands[args[0]]
	if !ok {
		help()
		os.Exit(-1)
		return
	}

	scopes := []string{"https://www.googleapis.com/auth/mobileinsights"}
	client, err := oauth2client.NewClient(secretBytes, nil)
	if err != nil {
		fmt.Printf("Failed to create OAuth2 client: %s\n", err)
		os.Exit(-1)
		return
	}
	token, err := client.GetToken(strings.Join(scopes, " "))
	if err != nil {
		fmt.Printf("Error getting token: %s\n", err)
		os.Exit(-1)
		return
	}

	cmdFunc(token)
}
